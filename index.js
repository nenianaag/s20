console.log("Hello, World!");

let number = Number(prompt('Give me a number.'));

console.log("The number you provided is " + number + ".");

for (let x = number; x > 0; x--){
	if (x <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} else if (x % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
	} else if(x % 5 === 0){
		console.log(x);
	}

};

let string = "supercalifragilisticexpialidocious";
let consonants = "";

for (let x = 0; x < string.length; x++) {
	if (string[x] === "a" || string[x] === "e" ||string[x] === "i" || string[x] === "o" || string[x] === "u"){ 
		continue;
	} else {
		consonants += string[x];
	}

}
console.log(string);
console.log(consonants);